import numpy as np

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
import keras

from string import ascii_lowercase


# TODO: fill out the function below that transforms the input series 
# and window-size into a set of input/output pairs for use with our RNN model
def window_transform_series(series, window_size):
    # containers for input/output pairs
    X = []
    y = []
    for i in range(len(series) - window_size):
        # inputs in each window are [x, x + window_size - 1].
        elements = series[i:i + window_size]
        X.append(elements)
        # output in each window is next to last element of inputs.
        y.append(series[i + window_size])

    # reshape each 
    X = np.asarray(X)
    X.shape = (np.shape(X)[0:2])
    y = np.asarray(y)
    y.shape = (len(y),1)

    return X, y


# TODO: build an RNN to perform regression on our time series input/output data
def build_part1_RNN(step_size, window_size):
    # from instruction, 1st layer's hidden unit is 5
    first_layer_hidden_units_num = 5

    # as the first layer in a Sequential model
    model = Sequential()
    model.add(LSTM(first_layer_hidden_units_num, input_shape=(window_size, 1)))
    # fully connected module with one unit
    model.add(Dense(1))

    # build model using keras documentation recommended optimizer initialization
    optimizer = keras.optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=1e-08, decay=0.0)

    # compile the model
    model.compile(loss='mean_squared_error', optimizer=optimizer)
    return model


### TODO: list all unique characters in the text and remove any non-english ones
def clean_text(text):
    # punctuation list whose elements should not be removed.
    proper_punctuations = ''.join([' ', '!', ',', '.', ':', ';', '?'])

    # acceptable chars
    proper_chars = ascii_lowercase + proper_punctuations

    # find all unique characters in the text
    non_proper_chars = ''.join(set([c for c in text if c not in proper_chars]))

    # remove as many non-english characters and character sequences as you can
    replaced_chars = ''.join([' ' for _ in range(len(non_proper_chars))])
    translation_table = text.maketrans(non_proper_chars, replaced_chars)
    text = text.translate(translation_table)

    # shorten any extra dead space created above
    text = text.replace('  ', ' ')
    return text


### TODO: fill out the function below that transforms the input text and window-size into a set of input/output pairs for use with our RNN model
def window_transform_text(text, window_size, step_size):
    # containers for input/output pairs
    inputs = []
    outputs = []

    for i in range(0, len(text) - window_size, step_size):
        # inputs in each window are [x, x + window_size - 1].
        elements = text[i:i + window_size]
        inputs.append(elements)
        # output in each window is next to last element of inputs.
        outputs.append(text[i + window_size])

    return inputs, outputs
